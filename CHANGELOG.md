# Dialob Documentation

## TODO

* Create/restructure new content for all empty .md pages
* Deprecate Expressions section and integrate content with Logic section. Separate documentation into logical chunks according to sub-topic.
* Deprecate Response Types section and integrate content with Basic Operations section.
* Complete typescript migration
* Implement search feature
* Remove Button component from Page title and replace with clickable logo icon
* Fix all broken images
* Fix anchor links
* MUI Styles updates for various components

---

## 1.0 (2021-01-12)

### Added
 
 * Markdown files added (complete and incomplete) to indicate general skeleton of documentation
 * Additional Typescript migration 

---

