## DEL functions and reserved words

The following sections will explain and give details on the use of DEL keywords and functions across multiple use cases.

[Overview of existing functions and reserved words](https://docs.dialob.io/functions-reserved-words/overview-functions-reserved-words)  
[General logic-building keywords in action](https://docs.dialob.io/functions-reserved-words/logic-in-action)  
[Time and date keywords and functions](https://docs.dialob.io/functions-reserved-words/time-date-keywords)  
[Language keywords](https://docs.dialob.io/functions-reserved-words/language-keywords)  