import main from './_index.md';
import workingwithtypes from './working-with-types.md';

const ResponseTypes = {
  name: 'Response Types', path: main,
  subs: [
    { name: 'Working with types', path: workingwithtypes },
  ]
}

export default ResponseTypes;